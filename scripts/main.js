

const API_GET_COUNTRIES_FROM_WORLD = 'https://api.worldbank.org/v2/country?format=json'; 

const API_GET_COUNTRIES_FROM_WORLD_V2 = 'https://restcountries.com/v3.1/all'; 
  
const getData = HttpRequest.get;

getData(API_GET_COUNTRIES_FROM_WORLD_V2)
    .then(res => {

        const title = document.getElementById('title')
        title.textContent = ` World countries (${res.length})`;

        res.forEach(element => {
            const { name, capital, flags, languages, region } = element;

            const { official } = name;
            const capitals = capital?.join(',');
            const languagesCountry = Object.values(languages).join(', ');
            const { png, alt } = flags;

            const divCol = document.createElement('div');
            const divCard = document.createElement('div');
            const imgCard = document.createElement('img');
            const divCardBody = document.createElement('div');
            const h4 = document.createElement('h4');
            const capitalText  = document.createElement('p');

            const viewOnMap = document.createElement('button');

            viewOnMap.setAttribute('type', 'button');
            viewOnMap.setAttribute('class', 'btn btn-secondary');
            viewOnMap.innerHTML = 'View map';

            divCol.className = 'col-sm-2 p-3';
            divCard.id = 'divCountry';
            divCard.className = 'card d-flex';
            divCard.style = 'width: 18rem;';
            divCardBody.className = 'card-body';

            imgCard.className = 'card-img-top';
            imgCard.src = png;
            imgCard.alt = alt;

            h4.className = 'card-title';
            h4.textContent = `${official} - ${region}`;
            capitalText.className = 'card-text';
            capitalText.innerHTML = `<strong> Capitals: </strong> ${capitals} <br> Language: ${languagesCountry}  `;

            divCol.appendChild(divCard);
            divCard.appendChild(imgCard);
            divCard.appendChild(divCardBody);

            divCardBody.appendChild(h4);
            divCardBody.appendChild(capitalText);
            divCardBody.appendChild(viewOnMap);

            const countryRow = document.getElementById('country');

            countryRow.appendChild(divCol);
        })
    }

    )
    .catch(
        e => console.error(e)
    )
    .finally( f => console.log("Finally ok"));


/*
getData(API_GET_COUNTRIES_FROM_WORLD)
    .then(res =>{
        const [ metadata , countriesData ] = res;

        console.log(metadata);
        console.log(countriesData);


        // Create elements for add to DOM of index

        countriesData.forEach( country => {

            let { name,  iso2Code, capitalCity } = country;

            if(capitalCity == '')  return;

            let urlFlag = `https://flagsapi.com/${iso2Code}/flat/64.png`;

            const divCol = document.createElement('div');
            const divCard = document.createElement('div');
            const imgCard = document.createElement('img');
            const divCardBody = document.createElement('div');
            const h4 = document.createElement('h4');
            const p  = document.createElement('p');

            divCol.className = 'col-sm-2 p-3';
            divCard.id = 'divCountry';
            divCard.className = 'card';
            divCard.style = 'width: 18rem;';
            divCardBody.className = 'card-body';

            imgCard.className = 'card-img-top';
            imgCard.src = urlFlag;
            imgCard.alt = name;

            h4.className = 'card-title';
            h4.textContent = name;
            p.className = 'card-text';
            p.textContent = 'Capital: '+capitalCity;

            divCol.appendChild(divCard);
            divCard.appendChild(imgCard);
            divCard.appendChild(divCardBody);

            divCardBody.appendChild(h4);
            divCardBody.appendChild(p);

            const countryRow = document.getElementById('country');

            countryRow.appendChild(divCol);
            console.log(country);
        } )

        // end


        
    })
    .catch(error => console.error(error))
    .finally(f => console.log('Execute finally'));

*/